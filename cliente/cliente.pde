/* --------------------------------------------------------------------------
 * SimpleOpenNI Hands3d Test
 * --------------------------------------------------------------------------
 * Processing Wrapper for the OpenNI/Kinect 2 library
 * http://code.google.com/p/simple-openni
 * --------------------------------------------------------------------------
 * prog:  Max Rheiner / Interaction Design / Zhdk / http://iad.zhdk.ch/
 * date:  12/12/2012 (m/d/y)
 * ----------------------------------------------------------------------------
 * This demos shows how to use the gesture/hand generator.
 * It's not the most reliable yet, a two hands example will follow
 * ----------------------------------------------------------------------------
 */
 
import java.util.Map;
import java.util.Iterator;


int handVecListSize = 20;
int x, y, w, h, speedX, speedY;
PGraphics pg;
float y1=50;
PrintWriter output;
int paddleXL, paddleYL, paddleW, paddleH, paddleS;
int paddleXR, paddleYR;
boolean upL, downL;
boolean upR, downR;
//colorL and colorR are not needed, but it can be helpful
color colorL = color(255,0,0);
color colorR = color(0, 255, 0, 50);
//scoreL is for left score and scoreR is for the right score
int scoreL = 0; 
int scoreR = 0;
//this sets the final winScore to a count of 10
int winScore = 10;
 

void setup()
{
//  frameRate(200);
  size(640,480);
  output = createWriter("../positions.txt");
  x = width/2; 
  y = height/2;
  w = 20;
  h = 20;
  //this allows the ball to bounce
  speedX = 2;
  speedY = 2;
  //this creates a text
  textSize(30);
  textAlign(CENTER, CENTER); //this centers the height and width of the text
  rectMode(CENTER); //this allows both paddles to be centered
  //This sets the paddles to a certain place on the window, and makes them visible
  paddleXL = 40;
  paddleYL = height/2;
  paddleXR = width-40;
  paddleYR = height/2;
  paddleW = 30;
  paddleH = 100;
  paddleS = 5;
  

 }

void draw()
{
  background(0);
  String[] lines = loadStrings("/../j.txt");
  println("there are " + lines.length + " lines");
  for (int i = 0 ; i < lines.length; i++) {
    y1= parseFloat(lines[i]);
  }
  barras();
  drawCircle();
  moveCircle();
  bounceOff();
  colision();
}
void barras(){
     fill(colorL);
     rect(40,y1,10,80);
     rect(540,y1,10,80);
} 

//creates circle
void drawCircle() {
  fill(0,0,255);
  ellipse(x, y, w, h);
}
//this allows the circle to bounce
void moveCircle() {  
  x = x + speedX*2;
  y = y + speedY*2;
}
 
//This detects if the ball has bounced or not
 
void bounceOff() {
 if ( x > width - w/2) {
    setup();
    speedX = -speedX;
    scoreL = scoreL + 1;
  } else if ( x < 0 + w/2) {
    setup();
    scoreR = scoreR + 1;
  }
  if ( y > height - h/2) {
    speedY = -speedY;
  } else if ( y < 0 + h/2) {
    speedY = -speedY;
  }
}

void colision(){
  if(x - w/2 < 45 && y- h/2 < y1 + 40 && y + h/2 > y1 - 40){
    if (speedX < 0) {
      speedX = -speedX*1;
    }
  }else if (x + w/2 > 535 && y - h/2 < y1 + 40 && y + h/2 > y1 - 540/2 ) {
    if (speedX > 0) {
      speedX = -speedX*1;
    }
  }
}
 
//This provides the user to see the score
 
void scores() {
  fill(255);
  text(scoreL, 100, 50);
  text(scoreR, width-100, 50);
}
 
//This determines who wins
 

 

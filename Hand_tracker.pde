/* --------------------------------------------------------------------------
 * SimpleOpenNI Hands3d Test
 * --------------------------------------------------------------------------
 * Processing Wrapper for the OpenNI/Kinect 2 library
 * http://code.google.com/p/simple-openni
 * --------------------------------------------------------------------------
 * prog:  Max Rheiner / Interaction Design / Zhdk / http://iad.zhdk.ch/
 * date:  12/12/2012 (m/d/y)
 * ----------------------------------------------------------------------------
 * This demos shows how to use the gesture/hand generator.
 * It's not the most reliable yet, a two hands example will follow
 * ----------------------------------------------------------------------------
 */
 
import java.util.Map;
import java.util.Iterator;

import SimpleOpenNI.*;

SimpleOpenNI context;
int handVecListSize = 20;
int x, y, w, h, speedX, speedY;
PGraphics pg;
float y1=50;
PrintWriter output;
int paddleXL, paddleYL, paddleW, paddleH, paddleS;
int paddleXR, paddleYR;
boolean upL, downL;
boolean upR, downR;
//colorL and colorR are not needed, but it can be helpful
color colorL = color(255,0,0);
color colorR = color(0, 255, 0, 50);
//scoreL is for left score and scoreR is for the right score
int scoreL = 0; 
int scoreR = 0;
//this sets the final winScore to a count of 10
int winScore = 10;
 
Map<Integer,ArrayList<PVector>>  handPathList = new HashMap<Integer,ArrayList<PVector>>();
color[]       userClr = new color[]{ color(255,0,0),
                                     color(0,255,0),
                                     color(0,0,255),
                                     color(255,255,0),
                                     color(255,0,255),
                                     color(0,255,255)
                                   };
void setup()
{
//  frameRate(200);
  size(640,480);
  output = createWriter("j.txt");
  x = width/2; 
  y = height/2;
  w = 50;
  h = 50;
  //this allows the ball to bounce
  speedX = 4;
  speedY = 4;
  //this creates a text
  textSize(30);
  textAlign(CENTER, CENTER); //this centers the height and width of the text
  rectMode(CENTER); //this allows both paddles to be centered
  //This sets the paddles to a certain place on the window, and makes them visible
  paddleXL = 40;
  paddleYL = height/2;
  paddleXR = width-40;
  paddleYR = height/2;
  paddleW = 30;
  paddleH = 100;
  paddleS = 5;
  

  context = new SimpleOpenNI(this);
  if(context.isInit() == false)
  {
     println("Can't init SimpleOpenNI, maybe the camera is not connected!"); 
     exit();
     return;  
  }   

  // enable depthMap generation 
  context.enableDepth();
  
  // disable mirror
  context.setMirror(true);

  // enable hands + gesture generation
  //context.enableGesture();
  context.enableHand();
  context.startGesture(SimpleOpenNI.GESTURE_WAVE);
  
  // set how smooth the hand capturing should be
  //context.setSmoothingHands(.5);
 }

void draw()
{
  // update the cam
  context.update();
 
  image(context.depthImage(),0,0);
  //background(0);
  //barras();
  //println(y1);
  // draw the tracked hands
  //println(handPathList.size());
  if(handPathList.size() > 0)  
  {    
    Iterator itr = handPathList.entrySet().iterator();     
    while(itr.hasNext())
    {
      
      Map.Entry mapEntry = (Map.Entry)itr.next(); 
      int handId =  (Integer)mapEntry.getKey();
      ArrayList<PVector> vecList = (ArrayList<PVector>)mapEntry.getValue();
      PVector p;
      PVector p2d = new PVector();
      
        stroke(userClr[ (handId - 1) % userClr.length ]);
        noFill(); 
        strokeWeight(1);        
        Iterator itrVec = vecList.iterator(); 
        //beginShape();
        //  while( itrVec.hasNext() ) 
        //  { 
        //    p = (PVector) itrVec.next(); 
            
        //    context.convertRealWorldToProjective(p,p2d);
        //    vertex(p2d.x,p2d.y);
        //  }
        //endShape();   
  
        //stroke(userClr[ (handId - 1) % userClr.length ]);
        //strokeWeight(4);
        p = vecList.get(0);
        context.convertRealWorldToProjective(p,p2d);
        //point(p2d.x,p2d.y);
        y1 = p2d.y;
        output.println(y1);
        output.flush(); // Writes the remaining data to the file
 
    }        
  }
}
void barras(){
     fill(colorL);
     rect(40,y1,10,80);
     rect(540,y1,10,80);
} 

//creates circle
void drawCircle() {
  fill(0,0,255);
  ellipse(x, y, w, h);
}
//this allows the circle to bounce
void moveCircle() {  
  x = x + speedX*2;
  y = y + speedY*2;
}
 
//This detects if the ball has bounced or not
 
void bounceOff() {
 if ( x > width - w/2) {
    setup();
    speedX = -speedX;
    scoreL = scoreL + 1;
  } else if ( x < 0 + w/2) {
    setup();
    scoreR = scoreR + 1;
  }
  if ( y > height - h/2) {
    speedY = -speedY;
  } else if ( y < 0 + h/2) {
    speedY = -speedY;
  }
}
 
//This provides the user to see the score
 
void scores() {
  fill(255);
  text(scoreL, 100, 50);
  text(scoreR, width-100, 50);
}
 
//This determines who wins
 

 

// -----------------------------------------------------------------
// hand events

void onNewHand(SimpleOpenNI curContext,int handId,PVector pos)
{
  println("onNewHand - handId: " + handId + ", pos: " + pos);
 
  ArrayList<PVector> vecList = new ArrayList<PVector>();
  vecList.add(pos);
  
  handPathList.put(handId,vecList);
}

void onTrackedHand(SimpleOpenNI curContext,int handId,PVector pos)
{
  println("onTrackedHand - handId: " + handId + ", pos: " + pos );
  
  ArrayList<PVector> vecList = handPathList.get(handId);
  if(vecList != null)
  {
    vecList.add(0,pos);
    if(vecList.size() >= handVecListSize)
      // remove the last point 
      vecList.remove(vecList.size()-1); 
  }  
}

void onLostHand(SimpleOpenNI curContext,int handId)
{
  println("onLostHand - handId: " + handId);
  handPathList.remove(handId);
}

// -----------------------------------------------------------------
// gesture events

void onCompletedGesture(SimpleOpenNI curContext,int gestureType, PVector pos)
{
  println("onCompletedGesture - gestureType: " + gestureType + ", pos: " + pos);
  
  int handId = context.startTrackingHand(pos);
  println("hand stracked: " + handId);
}

// -----------------------------------------------------------------
// Keyboard event
void keyPressed()
{

  switch(key)
  {
  case ' ':
    context.setMirror(!context.mirror());
    break;
  case '1':
    context.setMirror(true);
    break;
  case '2':
    context.setMirror(false);
    break;
  }
}
